const express = require('express')
const path = require('path')
const moment = require('moment')
const fs = require('fs')

const app = express()
let PORT = 3000;

let devices = []

app.use('/static', express.static('statics'))
app.use(express.urlencoded({
  extended: true
}))

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'templates', 'main.html'))
})

app.get('/forget_all', (req, res) => {
  devices = []
  res.send('Ack')
})

app.post('/registerDevice', (req, res) => {
  if (req.body.ip && req.body.hostname && req.body.hwid){
    let exists = false
    devices.forEach(e => {
      if (e.hwid == req.body.hwid) {
        exists = true
        e.ip = req.body.ip
        e.name = req.body.hostname,
        e.seen = moment()
      }
    })
    if (!exists) devices.push({
      name: req.body.hostname,
      hwid: req.body.hwid,
      ip: req.body.ip,
      seen: moment()
    })
    console.log(req.body.hostname)
    console.log(req.body.hwid)
    res.send('Ack')
  } else {
    res.send('Nack')
  }
})

app.get('/getdevices', (req, res) => {
  res.send(JSON.stringify(devices))
})

fs.readFile('settings.json', 'utf8', function (err, data) {
  if (err)
  {
    console.log('No settings file, using port 3000')
  } else {
    let settings = JSON.parse(data)
    PORT = settings.port
  }

  app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}!`)
  })
})