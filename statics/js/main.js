const SERVER_HOSTNAME = 'passivevlp.natfaulk.com'

let app = angular.module('myApp', [])
app.controller('passive', ['$scope', '$http', '$timeout', function($s, $http, $timeout) {
  console.log('this ran')  
  $s.data = ''
  $s.message = ''
  $s.devices = []

  $s.collectall = () => {
    $s.devices.forEach(element => {
      $s.collect(element)
    })
  }

  $s.forgetall = () => {
    $http.get(`http://${SERVER_HOSTNAME}/forget_all`).then((res) => {
      if (res.data === 'Ack') {
        console.log ('Forgot all devices')
        $s.devices = []
      }
      else console.log('Error forgetting all devices')
    }, (err) => {
      console.log(err)
      console.log('Error forgetting all devices')
    })
  }

  $s.collect = (_device) => {
    $s.dev_begin(_device.ip, (err, data) => {
      if (err) console.log('Error: ' + err.message)
      else if (data == 'Ack') {
        console.log('got ack')
        $s.message = 'Collecting...'
        // should take 10 seconds, timeout for 12 in case
        $timeout(() => {
          $s.dev_check(_device.ip, (err, data) => {
            if (err) console.log('Error: ' + err.message)
            else if (data == 'true') {
              $s.dev_get(_device.ip, (err, data) => {
                if (err) console.log('Error: ' + err.message)
                else {
                  _device.data = data
                  _device.message = 'Collection complete'
                }
              })
            } else {
              console.log('not true')
              $s.message = 'Collection failed'
            }
          })
        }, 12000)
      } else {
        $s.message = 'Collection failed'
        console.log('not ack')
      }
    })
  }

  $s.getReq = (_host, _path, _callback) => {
    $http.get(`http://${_host}/${_path}`).then((res) => {
      _callback(null, res.data)
    }, (err) => {
      _callback(err, null)
    })
  }

  $s.getReq(SERVER_HOSTNAME, 'getdevices', (err, data) => {
    if (err) console.log('Error: ' + err.message)
    else {
      $s.devices = data
      $s.devices.forEach(element => {
        element.data = []
        element.message = ''
        element.seenpretty = moment(element.seen).fromNow()
      })
    }
  })

  $s.dev_begin = (_ip, _callback) => {
    $s.getReq(_ip, 'begin', (err, data) => {
      _callback(err, data)
    })
  }

  $s.dev_check = (_ip, _callback) => {
    $s.getReq(_ip, 'checkdone', (err, data) => {
      _callback(err, data)
    })
  }

  $s.dev_get = (_ip, _callback) => {
    $s.getReq(_ip, 'getdata', (err, data) => {
      _callback(err, data)
    })
  }
}])

